---
title: "Système de noms de domaine"
date: 2023-10-18T23:48:47-04:00
draft: false
weight: 81
---

Le DNS permet de traduire les noms de domaine internet (e.g. *www.collegemv.qc.ca*) en adresses IP. La **résolution de noms** consiste à trouver l’adresse IP associée à un nom donné pour n’importe quel hôte sur un réseau local ou étendu.

Chaque hôte sur un réseau a une adresse IP. Cette adresse est essentielle à la communication entre lui-même et le reste du monde. Cependant, lorsque nous utilisons des services sur un réseau, il est beaucoup plus simple d'utiliser des noms: il faut donc trouver un moyen d’associer les adresses IP aux noms des hôtes qui ont ces adresses. 

Aux débuts d'internet, la liste de toutes les correspondances nom/adresse pour les hôtes ayant une adresse publique était maintenue à jour sur un serveur publi dans un fichier nommé HOSTS.TXT, et cette liste était téléchargeable par tous les hôtes qui pouvaient en avoir besoin. Assez rapidement, ce système est devenu difficile à gérer; on a donc inventé le DNS pour pallier les problèmes causés par un fichier centralisé. 

> Une archive d'anciennes versions des fichiers HOSTS.TXT est maintenue par un utilisateur GitHub: https://github.com/ttkzw/hosts.txt

L’objectif du DNS (*Domain Name System*) est d’avoir un système unique où:
+ L'information est décentralisée
+ La responsabilité de maintenir ces informations à jours est décentralisée

Les noms du DNS sont organisés en une hiérarchie:

![arbredns](/420-312/images/arbredns.jpg)
(Source: http://notionsinformatique.free.fr/reseaux/dns.html)

+ Un **domaine** est un nœud de la hiérarchie et tous ses descendants: `.com` est un domaine, `qc.ca` est un domaine, `collegemv.qc.ca` est un domaine, etc.
+ Les **hôtes** sont les machines qui ont une adresse IP dans un domaine. Par exemple dans le domaine `collegemv.qc.ca` il y a un hôte nommé `www`.
+ Les **zones** sont les limites de la responsabilité administrative. Dans chaque zone il y a un ou plusieurs serveurs DNS où sont enregistrées les adresses des hôtes dans cette zone. Par exemple, le domaine `.ca` contient le domaine `collegemv.qc.ca`, mais comme ces deux domaines sont administrés par des organisations différentes, ils font partie de zones différentes. Les limites de la zone définissent les limites de l'*autorité* d'une organisation sur une partie du domaine.
+ La **racine** est le sommet de la hiérarchie; elle contient les serveurs DNS du plus haut niveau, les *serveurs racine*. Ils sont désignés par une lettre de **a** à **m**, à l’url *[lettre].root-servers*.net. Certains ont une page web, par exemple `c.root-servers.org`.
+ La **délégation** est le fait que dans chaque zone, les serveurs DNS connaissent le nom et l'adresse des serveurs DNS des zones immédiatement en-dessous. Par exemple, un serveur DNS de la zone `.com` connaîtra l'adresse des serveurs DNS des zones `google.com`, `apple.com`, `airbus.com`, etc.

## nslookup

La commande `nslookup` permet de lancer des requêtes DNS. On peut l'utiliser pour obtenir les informations suivantes:
+ Adresse IP à partir du nom: `nslookup www.linux.org`
+ Nom à partir de IP: `nslookup 132.207.6.35`
+ Demander à un serveur spécifique: `nslookup www.linux.org 8.8.8.8`
+ Connaître le serveur DNS d'une zone : `nslookup -type=ns linux.org`

Dans la réponse, `Server` désigne le serveur qui répond à notre requête, `Address` est son adresse, et ensuite on peut avoir une ou plusieurs adresses IP (version 4 ou version 6):

```bash
ju@boum:~$ nslookup www.linux.org 192.168.50.1
Server:		192.168.50.1
Address:	192.168.50.1#53

Non-authoritative answer:
Name:	www.linux.org
Address: 104.26.14.72
Name:	www.linux.org
Address: 104.26.15.72
Name:	www.linux.org
Address: 172.67.73.26
Name:	www.linux.org
Address: 2606:4700:20::681a:e48
Name:	www.linux.org
Address: 2606:4700:20::ac43:491a
Name:	www.linux.org
Address: 2606:4700:20::681a:f48
```

Le terme `Non-authoritative answer` signifie que le serveur qui nous répond (ici, 192.168.50.1) n'a pas **autorité** sur la zone, c'est-à-dire qu'il n'est pas un des serveurs DNS de la zone `linux.org`.

{{% notice tip "Exercices" %}}
1. Quelle est l'adresse IP de `www.collegemv.qc.ca`?
2. Faites une requête inverse sur cette adresse. Quel nom obtenez-vous?
3. Quels sont les noms et les adresses IP des serveurs DNS pour `collegemv.qc.ca`?
4. Quelle est l'adresse IP de `espace.coop`?
5. Quelles sont les adresses IP des serveurs DNS pour `espace.coop`?
6. Quelles sont les adresses IP des serveurs DNS pour `qc.ca`?
7. Quelles sont les adresses IP des serveurs DNS pour `ca`?
8. Quelles sont les adresses IP des serveurs DNS pour la racine?
{{% /notice %}}

## Le chemin d'une requête DNS

Lorsqu'il reçoit une requête, le serveur connaît les adresses IP qu'on lui demande seulement si elles sont dans sa zone. Si elles sont dans une autre zone, il doit demander à d'autres serveurs. La procédure qu'il suit alors consiste à interroger les serveurs racine puis descendre la hiérarchie jusqu'au serveur qui a la réponse. Pour ne pas répéter ce processus à chaque requête (et ainsi éviter de surcharger les serveurs racine), la réponse sera conservée en mémoire pour une certaine période (on nomme cette période TTL, pour "Time To Live").

Par exemple, imaginons que vous êtes sur un PC du Collège et votre serveur DNS se nomme *sv33.collegemv.qc.ca*:
+ Vous tapez `www.gitlab.io` dans la barre d'adresse de Firefox
+ Une requête est envoyée au serveur *sv33.collegemv.qc.ca*
+ *sv33* n’est pas responsable de la zone `gitlab.io`; il ne possède donc pas l’information demandée.
+ *sv33* envoit lui-même une requête pour `www.gitlab.io` à un des 13 serveurs racine.
+ Le serveur racine retourne l’adresse IP d’un autre serveur DNS ayant autorité sur la zone `.io`.
+ *sv33* envoit une requête pour `www.gitlab.io` au serveur DNS de la zone `.io`
+ Le serveur DNS retourne l’adresse IP d’un autre serveur DNS, celui qui a l'autorité sur la zone `gitlab.io`
+ *sv33* envoit une requête pour `www.gitlab.io` au serveur DNS de la zone `gitlab.io`
+ Le serveur DNS retourne l’adresse IP de `www.gitlab.io`
+ *sv33* retourne l’adresse IP de `www.gitlab.io` à votre ordinateur et la met en cache.
+ Firefox accède au site.

{{% notice tip "dig" %}}
La commande `dig +trace www.gitlab.io` permet de simuler chacune de ces étapes.
{{% /notice %}}

## Récursif / itératif

Le client qui interroge initialement le serveur DNS émet une requête **récursive**; le serveur DNS qui interroge d'autres serveurs émet une requête **itérative**. 

La différence est dans la réponse que le serveur doit retourner: dans une requête récursive, le serveur doit retourner l’adresse IP *finale*; dans une requête itérative le serveur doit retourner l’adresse d’*un autre serveur DNS*.
