---
title: "Installation d'un serveur"
date: 2023-10-19T01:13:08-04:00
draft: false
weight: 82
---

Dans cette section nous verrons comment installer le service DNS sur un système Debian 11, créer des zones directes et inverses et y ajouter des enregistrements.

## Installation

Le serveur DNS de linux fait partie du paquetage *bind9*:

```bash
apt install bind9
```

Le paquetage *bind9* installe plusieurs fichiers de configuration dans `/etc/bind`. Il y a 2 catégories de fichiers:
+ Les fichiers de configuration du service, dont le nom commence par `named.conf`;
+ Les fichiers de définitions de zones, dont le nom commence par `db`;

Les fichiers `bind.keys` et `rndc.key` sont utilisés dans le cadre du protocole *dnssec*, une version sécurisée de DNS. Nous n'aborderons pas cette question dans le cadre du cours.

Chacun des fichiers et leur utilité sont décrits dans le tableau suivant.

| | |
|:---|----|
| **named.conf** | Le fichier de configuration lu en premier. Donne le nom des autres fichiers de configuration à inclure. |
| **named.conf.options** | Contient les options de configuration du serveur. |
| **named.conf.default-zones**| Réfère aux définitions de zones par défaut, qu'on devrait retrouver sur tous les serveurs DNS. |
| **named.conf.local** | Contient les définitions de zones sur lesquelles le serveur a autorité. Doivent être entrées par l'administrateur de la zone. |
| **zones.rfc1918** | Zones par défaut. |
| **db.0** | Zone par défaut. |
| **db.127** | Zone par défaut. |
| **db.255** | Zone par défaut. |
| **db.local** | Zone par défaut. |
| **db.empty** | Zone par défaut. |

À la base, notre serveur peut répondre aux requêtes DNS car il a peut contacter les serveurs racine et stocker en mémoire les réponses obtenues. On parle alors d'un serveur DNS « de cache ». 


