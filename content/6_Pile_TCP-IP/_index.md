+++
chapter = true
pre = "<b>6. </b>"
title = "Protocoles"
weight = 60
+++

### Module 6

# Protocoles et réseaux informatiques

![net](/420-312/images/net.png)

L'invention des réseaux informatiques, à partir des années 1960, a révolutionné la communication comme peu d'inventions avant elle. Sur une période de 30 ans, elle a donné naissance à internet puis au Web et a ainsi changé les habitudes de toutes les sociétés dans le monde. Dans cette section, nous verrons quelles sont les origines des réseaux informatiques et comment on utilise les protocoles pour structurer la communication sur ceux-ci.
