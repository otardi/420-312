+++
chapter = true
pre = "<b>1. </b>"
title = "Configuration d'un hôte"
weight = 10
+++

### Module 1

# Configuration d'un hôte

![hote](/420-312/images/host.png)
Dans ce module nous verrons quelles informations un PC (ou tout autre composant) doit connaître pour communiquer avec les autres sur un réseau informatique et comment en faire la configuration dans les environnements *Windows* et *linux*.