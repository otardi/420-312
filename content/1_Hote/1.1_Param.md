---
title: "1.1-Paramètres"
date: 2023-06-28T18:18:27-04:00
draft: false
weight: 11
---

Pour pouvoir communiquer normalement sur un réseau, un *hôte* doit avoir certaines informations. Dans cette section nous allons voir en quoi elles consistent et pourquoi elles sont utiles.

{{% notice info "Note" %}}
Un réseau informatique permet de connecter ensemble des ordinateurs « normaux » mais aussi toutes sortes d'autres composants, par exemple des tablettes, consoles de jeu, téléphones, caméras de surveillance, [grille-pains](https://techcrunch.com/2022/08/17/tineco-toasty-one/)... 

Le terme ***hôte*** est utilisé dans ce contexte pour désigner n'importe quel composant qui a la capacité de se connecter sur un réseau.
{{% /notice %}}

## Les éléments d'un réseau local

Le diagramme suivant correspond à un petit réseau local, similaire à celui que vous avez à la maison:

![homelan](/420-312/images/homelan.png)

Pour pouvoir communiquer entre eux, les hôtes de ce réseau ont besoin d'être identifiés par une adresse IP. Si par exemple un des ordinateurs portables veut faire imprimer un document, il doit envoyer une demande d'impression à l'hôte qui se trouve à l'adresse `192.168.1.10` (l'imprimante).

Remarquez que les adresses débutent toutes par `192.168.1` : le fait de partager le même préfixe permet aux hôtes de savoir qu'ils font partie du même réseau.

#### Passerelle

Lorsqu'un hôte veut communiquer avec un serveur qui se trouve à l'extérieur, comme par exemple pour faire une recherche sur *Google*, il doit sortir du réseau local. Pour ce faire il doit traverser la *passerelle* (en anglais, « gateway »), qui est sa porte de sortie vers internet. 

Donc si par exemple l'hôte ayant l'adresse `192.168.1.52` veut faire imprimer un document, il va tenter de contacter l'imprimante à `192.168.1.10`. Il sait que celle-ci est sur le même réseau que lui car leur adresse IP a le même préfixe. Si par contre le même hôte veut lancer une requête sur un serveur de *Google* à l'adresse `142.251.35.164`, il sait que celui-ci n'est pas sur le même réseau: il va donc envoyer son message vers la passerelle (`192.168.1.1`) en espérant que celle-ci l'envoie au bon endroit.

Tous les réseaux qu'on veut connecter sur internet (ou d'autres réseaux locaux) ont besoin d'avoir une passerelle, et les hôtes d'un réseau ont besoin de savoir quelle est l'adresse de cette passerelle pour communiquer avec l'extérieur.

#### Serveur DNS

Peu importe qu'un message soit destiné au réseau local ou à internet, pour qu'un message parvienne à destination on doit connaître l'adresse IP de celle-ci. Mais les adresses IP sont utilisées par des machines: lorsqu'on veut ouvrir une page web, on utilise un nom (par exemple `collegemv.omnivox.ca`); lorsqu'on envoie un courriel on utilise aussi un nom (`bob@collegemv.qc.ca`). Mais pour que les hôtes sachent à quelles adresses IP ces noms correspondent, il faut un système de traduction. C'est à cela que servent les serveurs DNS.

> DNS signifie _Domain Name System_ (« Système de noms de domaine »)

Chaque fois qu'on entre une adresse à la main, qu'on clique sur un lien, qu'on répond à un courriel, on utilise un nom pour identifier le serveur sur lequel on veut se connecter. Pour que notre message se rende à destination (et pour que la réponse nous revienne), c'est cependant l'adresse IP qui doit être utilisée par l'hôte. Si vous cliquez sur [ce lien](https://www.netflix.com/), votre hôte fera une requête à un serveur DNS et lui demandera à quelle adresse IP correspond *www.netflix.com*. Celui-ci lui répondra par exemple `52.3.144.142`, et votre hôte pourra envoyer sa demande de connexion à la page web.

Dans les petits réseaux comme celui qui est illustré plus haut, c'est souvent la passerelle qui remplit la fonction de serveur DNS. Dans les réseaux de plus grande taille, où les requêtes DNS peuvent être très nombreuses, on consacre un hôte distinct à cette fonction. Rien n'empêche cependant que le serveur DNS soit à l'extérieur du réseau local: il existe des serveurs DNS publics sur internet qui peuvent répondre aux requêtes de n'importe quel hôte.

Dans tous les cas, les réseaux ont besoin d'un serveur DNS, et les hôtes d'un réseau ont besoin de savoir quelle est l'adresse de ce serveur DNS pour l'interroger.

#### Serveur DHCP
Dans ce qui précède nous avons expliqué que les hôtes, pour communiquer normalement sur un réseau, ont besoin de connaître 3 informations:
+ Leur propre adresse IP
+ L'adresse IP de la passerelle
+ L'adresse IP d'un serveur DNS

Comment arrivent-ils à obtenir ces informations?

La réponse: un serveur DHCP leur fournit.

> DHCP signifie _Dynamic Host Configuration Protocol_ (« Protocole de configuration dynamique des hôtes »)

Il est évidemment possible pour un utilisateur d'entrer à la main ces informations. Si je le souhaite, je peux configurer l'adresse IP, la passerelle et le serveur DNS de chacun des hôtes sur mon réseau, mais ce serait long et je risquerais de faire des erreurs. Avec un serveur DHCP, ces informations peuvent être définies à un seul endroit (le serveur DHCP lui-même): ensuite chaque hôte, au moment où il démarre, envoit une requête DHCP au serveur qui lui retourne l'adresse IP qui lui sera attribuée, l'adresse de la passerelle et du serveur DNS, et au besoin plusieurs autres informations utiles sur le réseau.

Dans les petits réseaux, il est fréquent que la fonction de serveur DHCP soit prise en charge par le routeur (qui joue aussi déjà le rôle de serveur DNS).

##### Bail DHCP
L'ensemble des informations qu'un serveur DHCP envoit à ses clients se nomme « bail ». 

Les baux DHCP ont une durée de validité limitée: lorsqu'un bail arrive à la fin de sa validité, le client devra faire une nouvelle requête au serveur DHCP pour le renouveler. Il est possible à ce moment que les informations qu'il contient (comme l'adresse IP) changent.

##### Adresses statiques et dynamiques
Il y a donc deux manières de configurer les paramètres réseau d'un hôte: soit manuellement, soit par DHCP. Lorsque l'adresse IP d'un hôte est attribuée par DHCP, on dit qu'elle est _dynamique_ car elle est renouvelée régulièrement et il est possible qu'elle change lors du renouvellement du bail. Lorsqu'elle est entrée à la main, on dit qu'elle est _statique_.




