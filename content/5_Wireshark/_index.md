+++
chapter = true
pre = "<b>5. </b>"
title = "Wireshark"
weight = 50
+++

### Module 5

# Wireshark

![wslogo](/420-312/images/wslogo.png)


*Wireshark* est une application qui permet d’analyser le contenu des trames qui circulent sur un réseau. Une de ses grandes forces lui vient de sa capacité à reconnaître un très grand nombre de protocoles et à les présenter de manière structurée à l’utilisateur. On s’en sert généralement pour:

+ Observer le contenu de certains paquets spécifiques
+ Observer les conversations entre deux hôtes sur le réseau
+ Visualiser l’ensemble des messages pour un protocole spécifique
+ Dresser des statistiques sur l’utilisation d’un réseau

{{% notice info "Privilèges" %}}
Pour pouvoir capturer les paquets transmis, il faut avoir les privilèges système (*Administrateur* dans Windows ou *root* dans les systèmes linux). Vous devez donc exécuter le programme "en tant qu'administrateur" dans Windows ou avec `sudo` dans linux.
{{% /notice %}}

On peut utiliser *Wireshark* de deux manières. La première consiste à écouter en direct les paquets reçus et envoyés sur l'interface réseau de le lancer sur un hôte; la deuxième consiste à ouvrir un fichier qui contient des paquets déjà enregistrés.

## Démarrer une capture
Pour lancer une capture, il s'agit de sélectionner sur la page de départ l'interface que vous souhaitez écouter puis cliquer sur l'icône de démarrage:

![wiresharkstart](/420-312/images/wiresharkstart.png)

## Organisation de l'interface
L'interface se divise en trois sections.

![ws3pann](/420-312/images/ws3pann.png?height=400px)


La fenêtre du haut est la **liste des paquets**, avec leurs informations sommaires réparties dans chacune des colonnes: 
+ Adresse IP source 
+ Adresse IP de destination
+ Protocole du niveau supérieur
+ Taille en octets du paquet 
+ Informations sur le contenu

Les couleurs des lignes permettent de distinguer les protocoles ou les contenus des messages; ils sont basés sur des règles qu’on peut voir (et aussi modifier si on le souhaite) dans menu *View -> Coloring Rules*. 

La fenêtre du milieu contient les **détails du paquet** sélectionné dans la liste. Ces informations sont organisées en hiérarchie: chaque protocole utilisé dans le paquet est précédé d’un « + » et peut être déployé pour révéler des informations plus détaillées sur son contenu. 

La fenêtre du bas affiche les **octets du paquet** sélectionné, c'est-à-dire son contenu brut en octets. À gauche, chaque octet est représenté en hexadécimal (ou en binaire en cliquant à droite dans la fenêtre) et à droite par le caractère ASCII correspondant s'il existe. 

Des informations générales sur la capture sont affichées dans le bas de la fenêtre:
+ *Packets*: le nombre de paquets au total dans la capture
+ *Displayed*: le nombre de paquets affichés (car il est possible d'appliquer des filtres d'affichage)
+ *Dropped*: le nombre de paquets rejetés
+ *Ignored*: le nombre de paquets dont le contenu est ignoré

{{% notice tip "Exercices" %}}
*Pour faire cet exercice et les suivants, ouvrez le fichier [exemples_ws.pcapng](/420-312/exercices/exemples_ws.pcapng) dans Wireshark.*
1. Combien de paquets contient la capture?
2. À quelle adresse IP le paquet #45 est-il envoyé?
3. Le paquet #6 contient des informations pour quels protocoles ("Frame" n'est pas un protocole) ?
4. Dans le paquet #30, quelle est l'adresse MAC source?
5. Dans le paquet #627 pourquoi les adresses source et destination sont-elles des adresses MAC?
{{% /notice %}}

{{% expand "Solutions" %}}
1. 627
2. 31.13.71.33
3. Ethernet, IP (version 4), TCP
4. 00:21:29:68:7b:7c
5. Car il n'y a pas de données utilisables par le protocole IP dans ce paquet
{{% /expand %}}

## Sauvegarde
Lorsqu’on cesse la capture, il est ensuite possible de la sauvegarder sous un fichier ***.pcap***. Les fichiers *.pcap* peuvent ensuite être rouverts dans Wireshark. 

Il est également possible d’ouvrir des fichiers ayant été sauvegardés avec `tcpdump`, un utilitaire en ligne de commande dans linux.

## Analyse des paquets
Lorsqu'on clique-droit dans la fenêtre qui contient la liste des paquets, on accède au menu suivant:

![wspackcontext](/420-312/images/wspackcontext.png?height=400px)

Les fonctionnalités les plus utiles sont celles-ci:

#### *Mark/Unmark Packet*
Ajouter un "signet" à un paquet pour y revenir facilement par la suite sans avoir à défiler la liste. Lorsqu'on a plusieurs signets, `CTRL-SHIFT-B` (pour *back*) et `CTRL-SHIFT-N` (pour *next*) permettent d'en parcourir la liste.

#### *Set/Unset Time Reference*
Définir un paquet comme référence temporelle pour tous les autres paquets qui le suivent. Le paquet qui est ainsi sélectionné est identifié comme l'instant `REF` et la valeur de *Time* pour les autres paquets qui le suivent est le nombre de secondes depuis `REF`. 

#### *Conversation Filter -> IPv4*
Affiche uniquement les paquets ayant les deux adresses IP source et destination du paquet sélectionné.

#### *Follow -> TCP Stream*
Affiche uniquement les informations échangées dans la connexion TCP du paquet séletionné.

#### Détails du paquet
Lorsqu’on sélectionne un paquet dans la fenêtre du haut, on voit dans la fenêtre du milieu les informations globales pour chaque protocole qu’il contient. Pour inspecter les données de chacun de ces protocoles de façon plus approfondie, on clique sur la flèche à gauche. Les informations sont organisées en arborescence : on peut les « déplier » sur différents niveaux de profondeur.

Dans l'exemple suivant, on voit que le paquet #68 contient des informations relatives aux protocoles *Ethernet*, IPv4, *User Datagram Protocol* (UDP) et *Domain Name System* (DNS). Les informations Transmises par le protocole DNS sont *Transaction ID*, *Flags*, *Questions*, *Answers*, etc.:

![wshier](/420-312/images/wshier.png?height=300px)


{{% notice tip "Exercices" %}}
1. Combien de secondes séparent le paquet 100 du paquet 200?
2. Combien de temps au total dure la capture, en secondes?
3. Combien de paquets font partie de la conversation entre `192.168.1.108` et `184.24.145.29`?
4. Dans le paquet 505, quels sont les ports source et destination pour le protocole UDP?
{{% /notice %}}

{{% expand "Solutions" %}}
1. 0,731328
2. 33,46 secondes
3. 123
4. Source: 49944; Destination: 53
{{% /expand %}}

## Statistiques globales
Le menu ***Statistics*** donne accès à de nombreuses fonctionnalités utiles pour l’analyse des échanges capturés. En voici trois qui donnent des infrmations essentielles.

#### *Protocol Hierarchy* 
Affiche (en pourcentage et en nombre de paquets) la proportion de chacun des protocoles dans l'ensemble de la capture. Ceci est utile pour caractériser de manière globale le type de communications à analyser: les réseau est-il utilisé pour le web? Pour du streaming, pour des jeux en réseau? etc.

Les protocoles sont présentés comme une hiérarchie car les messages de différents protocoles "s'emboîtent" les uns dans les autres. Par exemple, les messages du protocole *Domain Name System* (DNS) sont contenus dans des messages du protocole *User Datagram Protocol* (UDP), qui sont contenus dans des paquets *Internet Protocol* V4 (IPv4), eux-même contenus dans des messages du protocole *Ethernet*...

![protoh](/420-312/images/protoh.png?height=300px)

Dans la fenêtre précédente, on peut faire les observations suivantes sur l'ensemble de la capture:
+ Les paquets IP transportent des messages de plusieurs protocoles différents: SSDP, QUIC, NetBIOS, Dropbox et DNS
+ 177 paquets (60%) sont des messages TCP ("transmission Control Protocol")
+ Le protocole TLS ("Transport Layer Security") compte pour 27,4% du nombre de paquets échangés mais plus de la moitié (51.0%) de la quantité de données échangées. 
+ etc.

#### *Endpoints*
Une communication sur un réseau a toujours lieu entre deux "endpoints" (ou *points terminaux*); cependant ceux-ci peuvent désigner plus d'une chose. 

Une interface réseau peut avoir plus d'une adresse IP, et un hôte identifié par une adresse IP peut faire rouler plusieurs applications différentes qui sont chacune en communication avec des serveurs différents. Lorsqu'on analyse les communications réseau avec Wireshark, on peut vouloir observer les communications à partir des interfaces, des adresses IP ou des applications; on voit donc qu'il peut exister au moins 3 types de "endpoints" différents.

L'interface réseau d'un PC physique est identifiée par une adresse MAC. Or c'est le protocole Ethernet qui utilise les adresses MAC, donc si on veut considérer les interfaces comme points terminaux on doit utiliser le protocole Ethernet. Si on veut voir les adresses IP différentes dans les paquets capturer, on utilise le protocole IP. Et si on veut voir les applications différentes qui utilisent le réseau on utilisera le protocole TCP.

![endpoints](/420-312/images/endpoints.png?height=400px)

Dans l'exemple précédent on a sélectionné l'onglet IPv4 donc ce sont les adresses IP qui sont les points terminaux. `Tx Packets` et `Tx Bytes` désignent le nombre (respectivement) de paquets et d'octets **envoyés**, et `Rx Packets` et `Rx Bytes` le nombre de paquets et d'octets **reçus** (On peut trier les colonnes en cliquant sur leur entête). On peut donc constater les faits suivants:
+ L'hôte `192.168.0.187` (qui est celui où la capture a été prise) a envoyé 66k de données et en a reçu 51k.
+ L'hôte à l'adresse `52.123.251.186` a envoyé 21 paquets et en a reçu 28 pour un total de 49.
+ etc.

#### *Conversations*
Les conversations affichent des statistiques similaires aux *endpoints* mais énumèrent toutes les *paires* de points terminaux ayant des données. Les colonnes **Rel Start** et **Duration** désignent respectivement à quel moment (en nombre de secondes) après le début de la capture la conversation débute, et la durée de la communication.

![conversations](/420-312/images/conversations.png?height=400px)

Dans l'exemple précédent on peut voir les statistiques suivantes:
+ Dans la communication entre `192.168.0.187` et `52.123.51.186` 31k ont été échangés en 0.32 secondes.
+ La conversation entre `192.168.0.187` et `52.108.8.12` a duré le plus longtemps.
+ La première conversation était entre `192.168.0.187` et `52.108.8.12` et n'était composée que de 4 paquets.
+ etc.

{{% notice tip "Exercices" %}}
1. Quel est le pourcentage de paquets du protocole HTTP sur l'ensemble de la capture? Le pourcentage d'octets transmis par HTTP?
2. Combien d'octets de données ont été reçus par `192.168.1.108`?
3. Combien de paquets ont été envoyés par `184.24.145.29`?
4. Quels sont les deux hôtes qui se sont échangés 64 paquets au total?
5. Quels sont les deux hôtes qui ont la conversation la plus longue?
{{% /notice %}}

{{% expand "Solutions" %}}
1. 21,7% et 63,8%
2. 73k
3. 61
4. `192.168.1.108` et `192.168.1.1`
5. `192.168.1.108` et `50.31.164.176`
{{% /expand %}}

## Filtres
Lorsqu'on cherche à analyser les communications dans une capture Wireshark, il est possible que celle-ci contienne des dizines, voire des centaines de milliers de paquets. Les filtres permettent de diminuer ce nombre en affichant uniquement les paquets qui correspondent à certains critères de recherche. Par exemple, on peut afficher seulement les paquets qui contiennent le protocole HTTP, seulement les paquets qui ont comme origine l'hôte `10.23.44.101`, etc. Il est par ailleurs possible de combiner plusieurs filtres.

#### Protocoles
Pour afficher les paquets d'un protocole spécifique, il s'agit simplement d'écrire le nom du protocole dans la barre du filtre. Dans l'exemple suivant on affiche les paquets contenant le protocole *SSDP*:

![wsfilt1](/420-312/images/wsfilt1.png?height=250px)

{{% notice info "Attention" %}}
Pour effacer le filtre il s'agit de cliquer sur le bouton "X" à droite de la barre de filtre.
{{% /notice %}}

#### Valeurs des entêtes
Dans un paquet de données, les informations utilisées par chaque protocole se trouvent dans ce qu'on appelle des **entêtes**. Par exemple, les adresse d'origine et de destination dans le protocole IP, l'identifiant d'une connexion dans le protocole TCP, le nom de l'hôte recherché dans une requête du protocole DNS, etc.

Les informations dans ces entêtes peuvent faire l'objet d'un filtre. Par exemple pour voir uniquement les paquets ayant pour origine l'hôte `192.168.0.187`, le filtre sera le suivant:

![wsfilt2](/420-312/images/wsfilt2.png?height=250px)

On peut aussi utiliser `ip.dst` pour l'adresse IP de destination, ou encore `ip.addr` pour les paquets dont l'adresse donnée est soit l'origine, soit la destination.

#### Opérateurs
On peut utiliser les opérateurs logiques (`or`, `and`, `!`) et arithmétiques (`==`,`!=`,`>`,`>=`,`<`,`<=`) pour spécifier des filtres plus complexes. Le tableau suivant donne quelques exemples:

| Filtre | Effet |
|---|---|
| `ip.addr != 10.10.10.10` | Les paquets dont l'adresse IP d'origine ou de destination est différente de 10.10.10.10 |
| `tcp.dst < 100` | Les paquets dont le port TCP de destination a une valeur inférieure à 100 |
|`http and tcp`|Les paquets qui contiennent les protocoles TCP et HTTP |
|`!(dns or icmp)`| Les paquets qui ne sont ni un DNS, ni ICMP |
| `udp and ip.src != 10.10.10.10` | Les paquets UDP dont la source est différente de 10.10.10.10 |

{{% notice info "Remarque" %}}
Il est possible d'appliquer facilement un filtre avec n'importe quelle information contenue dans la fenêtre des détails d'un paquet. Placez le pointeur de la souris sur l'information en question et cliquez-droit, puis sélectionnez dans le menu contextuel ***Apply as Filter -> Selected***: un filtre est alors automatiquement créé.
{{% /notice %}}

{{% notice tip "Exercices" %}}
Faites les filtres qui ont les effets suivants (le nombre entre parenthèses est le nombre de paquets affichés si vous avez fait le bon filtre):
1. Les paquets UDP qui viennent de l'hôte 192.168.1.108 (42)
2. Les paquets HTTP à destination de 208.80.154.224 ou 208.80.254.240 (5)
3. Les paquets qui utilisent un port TCP supérieur à 50000 (528)
4. Les paquets dont l'adresse IP source se situe entre 192.168.1.0 et 192.168.1.128 (358)
5. Les paquets des requêtes DNS qui pour le nom "fr.wikipedia.org" (2)
{{% /notice %}}

{{% expand "Solutions" %}}
1. `udp and ip.src==192.168.1.108`
2. http and (ip.dst==208.80.154.224 or ip.dst==208.80.254.240)
3. tcp.port > 50000
4. ip.src > 192.168.1.0 and ip.src < 192.168.1.130
5. dns.qry.name=="fr.wikipedia.org"
{{% /expand %}}

## À consulter
+ [Documentation officielle de *Wireshark*](https://www.wireshark.org/docs/)
+ [Tuto Wireshark](https://www.youtube.com/playlist?list=PLct_uZ8dtLRfgwAoLXgjn1ELmFCAi4dyg)


