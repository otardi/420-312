+++
chapter = true
pre = "<b>11. </b>"
title = "Windows Server"
weight = 100
+++

### Module 11

# Windows Server

## Introduction à Windows Server 2022 

C’est un produit Microsoft qui représente le dernier Système d’exploitation conçus spécifiquement pour les serveurs et qui offre une gamme de fonctionnalités et de services adaptés aux entreprises. Parmi les fonctionnalités adaptées aux entreprises on trouve Active Directory qui permet une meilleure gestion des utilisateurs, des ordinateurs et des groupes. 

