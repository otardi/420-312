+++
chapter = true
pre = "<b>2. </b>"
title = "Connexion au réseau"
weight = 20
+++

### Module 2

# Connexion au réseau

![câble rj45](/420-312/images/RJ45.jpg)
Dans ce module nous verrons les types de câbles Ethernet RJ-45, la fabrication de câbles, les concentrateurs et commutateurs ainsi que la connexion d'un RaspberryPi sur un commutateur.