---
title: "Serveur Apache"
date: 2023-10-29T12:22:06-04:00
draft: false
weight: 102
---

Dans ce qui suit nous verrons comment installer un serveur HTTP et configurer certaines de ses fonctionnalités de base dans Debian.

## Installation

Importez la VM *Debian11.ova* fournie.

Il existe plusieurs programmes de serveurs HTTP - *lighttpd*, *Apache* et *nginx* dans linux, *IIS* sur Windows... Ici, nous allons Installer le serveur **Apache v2**.

En tant que *root*, lancez la commande suivante:

```bash
root@debian:~# apt install apache2
```

Le serveur devrait démarrer dès son installation; pour vérifier, ouvrez à partir de votre poste de travail une page à l'adresse de votre VM. Vous devriez voir ceci:

![defapache](/420-312/images/defapache.png)

Trois répertoires sont utilisés par le serveur apache:
+ Configuration du serveur: `/etc/apache2`
+ Pages web: `/var/www/html`
+ Journaux systèmes: `/var/log/apache2`

## Configuration

Les fichiers qui permettent la configuration du serveur sont assez nombreux. En effet un serveur web peut supporter différents types de fonctions et de contenus, et le serveur Apache en particuler est conçu de façon modulaire, c'est-à-dire que l'ajout d'une fonctionnalité nécessite qu'on ajoute un module. Le répertoire `/etc/apache2` conteint les éléments suivants:

| | |
|--:|--|
| **apache2.conf** | Fichier de configuration principal. Contient les directives générales valides pour l’ensemble du serveur. | 
| **conf-available/** | Fichiers de configuration prédéfinis, généralement utilisés pour définir des paramètres de configuration spécifiques à des fonctionnalités. | 
| **conf-enabled/** | Fichiers de configuration activés (liens symboliques vers des fichiers de configuration situés dans *conf-available*) | 
| **envvars** | Variables d’environnement à l’usage du serveur. | 
| **magic** | Données permettant de détecter automatiquement le type de contenus des fichiers. | 
| **mods-available/** | Modules apache prédéfinis pour différents usages. | 
| **mods-enabled/** | Modules activés (liens symboliques vers des fichiers de configuration situés dans *mods-available*). | 
| **ports.conf** | Configuration des ports utilisés pour les connexions entrantes. |
| **sites-available/** | Définitions de sites prédéfinies pour différents usages. | 
| **sites-enabled/** | Définitions de sites activées (liens symboliques vers des fichiers de configuration situés dans *sites-available*). | 

Pour contrôler l’exécution du service, il faut utiliser la commande *systemctl*. Les actions possibles sont:
+ `status`: Voir l'état du service
+ `start`: Démarrer le service
+ `stop`: Arrêter le service
+ `restart`: Redémarrer le service
+ `reload`: Recharger les fichiers de configuration sans redémarrer

Par exemple, pour voir l'état du service:
```
● apache2.service - The Apache HTTP Server
     Loaded: loaded (/lib/systemd/system/apache2.service; enabled; vendor preset: enabled)
     Active: active (running) since Sat 2023-10-28 13:33:36 EDT; 30min ago
       Docs: https://httpd.apache.org/docs/2.4/
   Main PID: 1743 (apache2)
      Tasks: 55 (limit: 2280)
     Memory: 9.0M
        CPU: 128ms
     CGroup: /system.slice/apache2.service
             ├─1743 /usr/sbin/apache2 -k start
             ├─1745 /usr/sbin/apache2 -k start
             └─1746 /usr/sbin/apache2 -k start

Oct 28 13:33:36 debian systemd[1]: Starting The Apache HTTP Server...
```

#### Répertoire public

On nomme *racine* le répertoire qui contient les fichiers (HTML ou autres) servis par Apache. Dans la plupart des distributions linux, ce répertoire est `/var/www` ou un de ses sous-répertoires. C’est ici qu’on déposera les fichiers *html*, *css*, *php*, *js* ou autres qui vont constituer les sites web hébergés par le serveur. 

Le répertoire du système de fichier qui sera la racine du site est défini par le paramètre de configuration **ServerRoot**, dans le fichier `apache2.conf`. Sa valeur par défaut sur Debian est `/var/www/html`. 

Par défaut, la racine du site contient un seul fichier html (*index.html*) qui sera servi à tout client qui tente d’accéder au site sans spécifier explicitement un fichier. Si le fichier *index.html* est absent, le serveur affiche la liste de tous les fichiers contenus dans le répertoire. 

#### Création d'une page web

Pour créer une page sur le serveur il s'agit donc de mettre un document HTML dans la racine. Créez donc un fichier nommé **test.html** dans le répertoire `/var/www/html/` puis ajoutez-y le contenu suivant:

```html
<h1>Ceci est un test</h1>
```
Vous pouvez ensuite accéder à cette page en donnant son URL dans la barre d’adresse de votre navigateur:

![testapache](/420-312/images/testapache.png)