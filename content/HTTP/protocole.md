---
title: "Protocole HTTP"
date: 2023-10-29T12:21:39-04:00
draft: false
weight: 101
---

À la base, le service HTTP est simple : il s’agit de transmettre le contenu d’un fichier texte dont le chemin est spécifié dans la requête du client. Ce chemin est représenté dans une URL.  

Avant HTTP, l’accès à des fichiers sur un serveur distant passait par le protocole FTP, qui nécessitait de se connecter sur un serveur, donner un identifiant et un mot de passe, se déplacer dans la structure des répertoires puis télécharger le fichier, ensuite se déconnecter. Avec HTTP, il s’agit simplement de nommer le chemin complet vers un fichier pour accéder aux données qu’il contient. 

En plus du contenu du fichier lui-même, le protocole HTTP permet au serveur de transmettre au client, au moment de la réponse, diverses informations utiles comme par exemple:  
+ Encodage du texte (Latin, Arabe, Hangul, etc.); 
+ Format des données binaires (JPG, GIF, MP3, etc.); 
+ Cookie de session; 
+ Nom et version du serveur; 

À l’inverse, le client peut lui aussi fournir des informations au serveur au moment de la requête. Toutes ces informations, qui ne font pas partie du fichier transmis, ne sont pas visibles dans le navigateur web. Elles font partie de ce qu’on appelle les **en-têtes http**. Il y a donc une entête à la requête et une en-tête à la réponse. 

La structure des en-têtes et les informations qu’elles peuvent contenir sont définies dans les documents [RFC2616](https://www.rfc-editor.org/rfc/rfc2616) et [RFC4229](https://www.rfc-editor.org/rfc/rfc4229). 

Le port **TCP 80** est associé au protocole HTTP; les connexions sécurisées (HTTPS) utilisent le port **TCP 443**. 

## Éléments du protocole HTTP

HTTP, comme tout protocole de communication, définit les «tours de parole» entre les  participants (client et serveur), la façon de représenter les informations échangées, et aussi le vocabulaire qui les participants peuvent utiliser. 

En HTTP, les **méthodes** définissent le type d’information qu’un client peut demander au serveur; autrement dit: le type de requête. 

Les **entêtes** définissent des informations connexes à la requête elle-même, qui donnent au serveur des éléments contextuels lui permettant d’adapter sa réponse ou qui donnent au client des éléments pour mieux interpréter celle-ci. 

Les **codes HTTP** sont des valeurs numériques qui correspondent au statut d’une requête; elles comprennent entre autres les codes d’erreurs. 

#### Méthodes

Les méthodes définies dans la version 1.1 du protocole sont les suivantes: 
+ OPTIONS  
+ GET  
+ POST  
+ HEAD  
+ PUT 
+ DELETE   
+ TRACE  
+ CONNECT 

`GET` et `POST` sont de loin les plus utilisées. 

`GET` est la méthode utilisée lorsqu’un client demande au serveur de lui fournir une page à l’URL donnée. Par exemple, pour l’URL http://10.10.10.100/page2.html, on demande au serveur de nous envoyer les données contenues dans le fichier `page2.html` à la racine du site. La requête que le client envoit au serveur est la suivante: 
```
GET /allo.html HTTP/1.1\r\n 
```

Il est possible de configurer un serveur HTTP pour qu’il puisse interpréter le passage de paramètres dans une requête `GET` et exécuter un programme plutôt que de directement retourner le contenu d’un fichier. Par exemple, lorsqu’un serveur est configuré pour exécuter des pages PHP, l’URL désigne un «script» php; dans ce cas la requête `GET` peut contenir des paramètres qui seront passés au script. Dans ce cas les requêtes auront la forme suivante (les paramètres sont des paires attribut/valeur suivante le caractère « ? ») : 
```
GET /voirItem.php?id=344 HTTP/1.1\r\n 
```

`POST` permet également au client de transmettre des informations au serveur, mais cette fois-ci ces informations ne seront pas transmises dans l’URL : elles le seront dans des champs de données directement envoyées au serveur. 
> Voir par exemple le paquet 16 dans [ce fichier Wireshark](/420-312/exercices/HTTP_POST.pcapng).

#### Entêtes
Les entêtes HTTP («HTTP Headers» en anglais) permettent au client et au serveur de s’échanger des informations qui servent à interpréter la requête ou sa réponse ou encore de donner des informations contextuelles supplémentaires. Le protocole en définit 35; voici certaines d’entre elles:
##### Client
+ **Host**: Le nom du serveur à qui la requête est envoyée (par exemple *www.ethereal.com*) 
+ **User-Agent**: Informations permettant au serveur de reconnaître le client (par exemple Chrome, Mozilla etc.) 
+ **Accept**: Indique au serveur les types de médias (comme les formats de fichiers) que le client peut traiter ou accepter.
+ **Accept-Language**: Spécifier les langues préférées pour la réponse. Le serveur peut utiliser cette information pour générer du contenu dans la langue appropriée si cela est possible.
+ **Accept-Encoding**: Indique les méthodes de compression que le client peut traiter. Cela permet au serveur de compresser la réponse si le client le supporte, ce qui peut améliorer les performances en réduisant la quantité de données transférées.

##### Serveur
+ **Server**: Le programme correspondant au serveur (IIS, apache, etc.) 
+ **Last-Modified**: Indique la date et l'heure de la dernière modification du contenu de la page demandée. Cela peut être utile pour la mise en cache côté client et pour déterminer si le contenu a été modifié depuis la dernière demande.
+ **Content-Type**: Le type des données envoyées par le serveur (HTML, jpeg, mp3, etc.) 
+ **Content-Length**: Indique la taille en octets de la réponse. 
  
> Voir par exemple les paquets 4 (entêtes client) et 38 (entêtes serveur) dans [ce fichier Wireshark](/420-312/exercices/http.pcap).

#### Codes HTTP
Chaque requête reçue par un serveur HTTP génère une réponse; les codes de statut HTTP sont des valeurs numériques qui correspondent à toutes les réponses possible qu’un serveur peut envoyer à un client. Par exemple, un code existe si aucune page ne correspond à la requête reçue; un autre existe pour une redirection vers un autre serveur; etc. Leur nombre est [très élevé](https://developer.mozilla.org/fr/docs/Web/HTTP/Status), mais voici les plus fréquents: 
+ **200**: *OK*: La requête est acceptée et le contenu du fichier correspondant est envoyé au client 
+ **301**: *Moved Permanently*: redirection vers une autre URL 
+ **400**: *Bad Request*: la requête envoyée par le client n’est pas correctement formée 
+ **403**: *Forbidden*: Le fichier demandé n’a pas les droits de lecture 
+ **404**: *Not Found*: Le fichier demandé n’existe pas 
+ **500**: *Internal Server Erro*r: Une erreur inconnue est survenue et il est impossible de transmettre les données 

