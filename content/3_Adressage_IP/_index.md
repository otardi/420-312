+++
chapter = true
pre = "<b>3. </b>"
title = "Adressage IP"
weight = 30
+++

### Module 3

# Introduction au protocole IP

![ipbox](/420-312/images/ipbox.png)

IP signifie *Internet Protocol*; c'est le protocole utilisé pour faire circuler les [datagrammes](https://vitrinelinguistique.oqlf.gouv.qc.ca/fiche-gdt/fiche/2076501/datagramme-ip) entre les réseaux interconnectés. Il existe actuellement deux versions du protocole IP: v4 et v6. Dans ce module nous verrons comment fonctionnent les adresses IPv4 et comment elles sont utilisées pour identifier les réseaux et les hôtes qui s'y trouvent.